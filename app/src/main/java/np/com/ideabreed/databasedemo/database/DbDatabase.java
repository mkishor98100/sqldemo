package np.com.ideabreed.databasedemo.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import np.com.ideabreed.databasedemo.models.DBUserModel;

import static np.com.ideabreed.databasedemo.utils.ConstantsData.DATABASE_NAME;

@Database(entities = {DBUserModel.class}, exportSchema = false, version = 1)
public abstract class DbDatabase extends RoomDatabase {
    private static DbDatabase instance = null;

    public static DbDatabase getInstance(Context context){

        if(instance == null) {

            instance = Room.databaseBuilder(context, DbDatabase.class, DATABASE_NAME)
                           .fallbackToDestructiveMigration()
                           .allowMainThreadQueries()
                           .build();

        }
        return instance;

    }

    public abstract DAO dao();
}
