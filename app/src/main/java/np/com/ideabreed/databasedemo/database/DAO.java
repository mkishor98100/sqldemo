package np.com.ideabreed.databasedemo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import np.com.ideabreed.databasedemo.models.DBUserModel;

@Dao
public interface DAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void inserUserIntoDb(DBUserModel... dbUserModels);

    @Query("SELECT * FROM users_demo")
    List<DBUserModel> getAllUsers();

    @Query("SELECT * FROM users_demo WHERE id =:id")
    DBUserModel getOneData(int id);

    @Query("DELETE FROM users_demo")
    void deleteAllUsers();

    @Query("DELETE FROM users_demo WHERE id=:id")
    void deleteOneUser(int id);

    @Delete
    void deleteUser(DBUserModel userModel);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateUser(DBUserModel userModel);


}
