package np.com.ideabreed.databasedemo.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import np.com.ideabreed.databasedemo.models.UserModel;

import static np.com.ideabreed.databasedemo.utils.ConstantsData.ADDRESS;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.CONTACT;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.DATABASE_NAME;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.DATABASE_VERSION;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.EMAIL;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.ID;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.IMAGE_URL;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.NAME;
import static np.com.ideabreed.databasedemo.utils.ConstantsData.USERS_TABLE;

public class DatabaseHandler extends SQLiteOpenHelper {
    private Context context;

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    //insert all data
    public void insertPostIntoDb(UserModel userModel){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(NAME, userModel.getName());
        contentValues.put(ADDRESS, userModel.getAddress());
        contentValues.put(EMAIL, userModel.getEmail());
        contentValues.put(CONTACT, userModel.getContact());
        contentValues.put(IMAGE_URL, userModel.getImage_link());

        long id = db.insert(USERS_TABLE, null, contentValues);
        db.close();


    }

    //fetch all data
    public List<UserModel> getAllUsers(){

        List<UserModel> userModelList = new ArrayList<>();
        String sql = "SELECT * FROM " + USERS_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);

        if(cursor.moveToFirst()) {
            do {

                UserModel model = new UserModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setName(cursor.getString(1));
                model.setAddress(cursor.getString(2));
                model.setEmail(cursor.getString(3));
                model.setContact(cursor.getString(4));
                model.setImage_link(cursor.getString(5));


                userModelList.add(model);


            } while(cursor.moveToNext());


        }

        cursor.close();
        return userModelList;


    }


    //fetch data by id
    public UserModel getOneData(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(USERS_TABLE, new String[]{ID, NAME, ADDRESS, EMAIL, CONTACT,
                                                           IMAGE_URL
        }, ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);

        if(cursor != null) { cursor.moveToFirst(); }

        UserModel test = new UserModel(Integer.parseInt(cursor.getString(0)), cursor.getString(1)
                , cursor
                .getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));

        cursor.close();

        return test;


    }

    //update data

    public int updateData(UserModel userModel){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, userModel.getName());
        contentValues.put(ADDRESS, userModel.getAddress());
        contentValues.put(EMAIL, userModel.getEmail());
        contentValues.put(CONTACT, userModel.getContact());
        contentValues.put(IMAGE_URL, userModel.getImage_link());


        int res = db.update(USERS_TABLE, contentValues, ID + " =?", new String[]{
                String.valueOf(userModel.getId())


        });
        db.close();
        return res;


    }


    //delete data

    public int deleteData(int id){
        SQLiteDatabase db = this.getWritableDatabase();

        int res = db.delete(USERS_TABLE, ID + "=?", new String[]{String.valueOf(id)});


        db.close();
        return res;


    }

    @Override
    public void onCreate(SQLiteDatabase db){

        String SQL_QUERY = "CREATE TABLE " + USERS_TABLE + "(" + ID + " INTEGER PRIMARY KEY " +
                "AUTOINCREMENT ," + NAME + " " + "VARCHAR(50)," + ADDRESS + " VARCHAR(200)," + EMAIL + " VARCHAR(200)," + CONTACT + " VARCHAR(10)," + IMAGE_URL + " VARCHAR(200)" + ")";
        db.execSQL(SQL_QUERY);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1){

        db.execSQL("DROP TABLE IF EXISTS " + USERS_TABLE);
        onCreate(db);

    }

}
