package np.com.ideabreed.databasedemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import np.com.ideabreed.databasedemo.database.DatabaseHandler;
import np.com.ideabreed.databasedemo.database.DbDatabase;
import np.com.ideabreed.databasedemo.models.DBUserModel;

public class EditActivity extends AppCompatActivity {
    EditText nameInput, addressInput, emailInput, contactInput, imageInput;
    DatabaseHandler db;
    DbDatabase db1;
    Button button;
    DBUserModel model, data;
    Intent intent;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        db = new DatabaseHandler(this);
        db1 = DbDatabase.getInstance(this);
        nameInput = findViewById(R.id.update_name);
        addressInput = findViewById(R.id.update_address);
        emailInput = findViewById(R.id.update_email);
        contactInput = findViewById(R.id.update_contact);
        imageInput = findViewById(R.id.update_image_link);
        button = findViewById(R.id.updatebutton);
        model = new DBUserModel();

        intent = getIntent();
        id = intent.getIntExtra("id", 0);
        data = db1.dao().getOneData(id);

        nameInput.setText(data.getName());
        addressInput.setText(data.getAddress());
        emailInput.setText(data.getEmail());
        contactInput.setText(data.getContact());
        imageInput.setText(data.getImage_link());


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                String name = nameInput.getText().toString();
                String address = addressInput.getText().toString();
                String email = emailInput.getText().toString();
                String contact = contactInput.getText().toString();
                String image = imageInput.getText().toString();

                model.setId(id);
                model.setName(name);
                model.setAddress(address);
                model.setEmail(email);
                model.setContact(contact);
                model.setImage_link(image);

                db1.dao().updateUser(model);

                //Alternative method
                //                db1.dao().deleteOneUser(id);
                //                db1.dao().inserUserIntoDb(model);


                Toast.makeText(EditActivity.this, "successfully updated", Toast.LENGTH_SHORT)
                     .show();


            }
        });


    }
}
