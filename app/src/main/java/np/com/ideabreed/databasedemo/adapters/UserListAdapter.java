package np.com.ideabreed.databasedemo.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import np.com.ideabreed.databasedemo.EditActivity;
import np.com.ideabreed.databasedemo.R;
import np.com.ideabreed.databasedemo.database.DatabaseHandler;
import np.com.ideabreed.databasedemo.database.DbDatabase;
import np.com.ideabreed.databasedemo.models.DBUserModel;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> {

    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    DatabaseHandler db;
    DbDatabase db1;
    private Context context;
    private List<DBUserModel> userModelList;

    public UserListAdapter(Context context){
        this.context = context;
        db = new DatabaseHandler(this.context);
        db1 = DbDatabase.getInstance(context);
    }

    public void setData(List<DBUserModel> userModels){
        this.userModelList = userModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.user_item, parent, false);

        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position){

        DBUserModel model = userModelList.get(position);

        holder.id.setText(String.valueOf(model.getId()));
        holder.name.setText(model.getName());
        holder.address.setText(model.getAddress());

        Glide.with(context).load(Uri.parse(model.getImage_link())).into(holder.imageView);


    }

    @Override
    public int getItemCount(){
        return userModelList.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView, editBtn, deleteBtn;
        TextView id, name, address;


        public UserViewHolder(@NonNull View itemView){
            super(itemView);

            imageView = itemView.findViewById(R.id.users_image);
            editBtn = itemView.findViewById(R.id.edit_click);
            deleteBtn = itemView.findViewById(R.id.delete_click);

            id = itemView.findViewById(R.id.user_id);
            name = itemView.findViewById(R.id.user_name);
            address = itemView.findViewById(R.id.user_address);


            editBtn.setOnClickListener(this);
            deleteBtn.setOnClickListener(this);


        }

        @Override
        public void onClick(View view){

            int position = getAdapterPosition();
            final DBUserModel model = userModelList.get(position);

            if(view.getId() == R.id.edit_click) {

                Intent intent = new Intent(context, EditActivity.class);
                intent.putExtra("id", model.getId());
                context.startActivity(intent);


            } else if(view.getId() == R.id.delete_click) {

                builder = new AlertDialog.Builder(context);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i){

                        //                        db.deleteData(model.getId());

                        db1.dao().deleteUser(model);
                        //Alternative method
                        //                        db1.dao().deleteOneUser(model.getId());

                        setData(db1.dao().getAllUsers());

                        Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i){
                        Toast.makeText(context, "delete failed", Toast.LENGTH_SHORT).show();

                        alertDialog.dismiss();

                    }
                });
                alertDialog = builder.create();
                alertDialog.setTitle("Delete User");
                alertDialog.setMessage("Are you sure to delete?");
                alertDialog.show();


            }


        }
    }


}
