package np.com.ideabreed.databasedemo.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import np.com.ideabreed.databasedemo.R;
import np.com.ideabreed.databasedemo.adapters.UserListAdapter;
import np.com.ideabreed.databasedemo.database.DatabaseHandler;
import np.com.ideabreed.databasedemo.database.DbDatabase;
import np.com.ideabreed.databasedemo.models.DBUserModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    RecyclerView recyclerView;
    UserListAdapter adapter;
    DatabaseHandler db;
    DbDatabase db1;
    private List<DBUserModel> userModelList;


    public HomeFragment(){
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        db = new DatabaseHandler(getContext());
        db1 = DbDatabase.getInstance(getContext());
        userModelList = db1.dao().getAllUsers();

        recyclerView = view.findViewById(R.id.users_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new UserListAdapter(getContext());
        adapter.setData(userModelList);

        recyclerView.setAdapter(adapter);


    }
}
